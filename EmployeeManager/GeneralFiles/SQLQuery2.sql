/**
This script is only an alternative to generate simulated employees, when generating employee with the same name the filters do not apply, it is only a sample of how to do it from MS SQL Transact TSQL.
For: JISC
*/

-- Clean database
IF(EXISTS (SELECT * FROM Employees))
BEGIN
	TRUNCATE TABLE Employees
END
IF(EXISTS (SELECT * FROM Deparments))
BEGIN
	DELETE FROM Deparments
	DBCC CHECKIDENT ('Deparments', RESEED, 0);
END


-- Create departments
INSERT INTO Deparments(Name) VALUES
	('Recursos Humanos'),('Contabilidad'),('Proyectos'),('Almacén'),('Seguridad'),
	('Desarrollo'),('Innovación tecnológica'),('Gerencia'),('Finanzas'),('Mantenimiento')

-- Create employees random
DECLARE @LimitEmployees INT = 100
DECLARE @IdSql INT = 1
DECLARE @EmployeeName varchar(100) = 'EMPLEADO DEMO'
DECLARE @Phone varchar(15) = '492105'
DECLARE @Email varchar(50) = 'email@mail.com'


WHILE(@IdSql <= @LimitEmployees)
BEGIN
	-- Set general propierties
	SET @EmployeeName = 'EMPLEADO DEMO'
	SET @EmployeeName = @EmployeeName + ' ' + Convert(varchar(10),@IdSql)
	SET @Email = 'email' + CONVERT(varchar(10), @IdSql) + '@gmail.com'
	SET @Phone = '492105'
	SET @Phone = @Phone + FORMAT(@IdSql, '0000')

	-- Random number for department
	DECLARE @DeparmentID BIGINT
	DECLARE @maxid INT = (SELECT MAX(DeparmentId) FROM Deparments)
	SELECT @DeparmentID = ABS((CHECKSUM(NEWID()))) % @maxid;
	IF(@DeparmentID = 0)
	BEGIN
		SET @DeparmentID = (SELECT TOP 1 DeparmentId FROM Deparments)
	END

	-- Insert data
	INSERT INTO Employees([Name],[DeparmentId],[Phone],[Email]) VALUES(@EmployeeName, @DeparmentID, @Phone, @Email)
	
	-- Increment counts
	SET @IdSql = @IdSql + 1
END


SELECT 'Process completed successfully. ' as Status