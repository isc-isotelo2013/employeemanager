﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EmployeeManager.Models
{
    /// <summary>
    /// Map model to employee table int the database, it´s use the anotations data and config relationship with table Deparments
    /// </summary>
    public class Employee
    {
        public int EmployeeId { get; set; }
        [DisplayName("Nombre")]
        [Required]
        public string Name { get; set; }
        [DisplayName("Telefono")]
        public string Phone { get; set; }
        [DisplayName("Correo electronico")]
        public string Email { get; set; }
        [Required]
        public int DeparmentId { get; set; }
        public Deparment Deparment { get; set; }
    }
}