﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EmployeeManager.Models
{
    /// <summary>
    /// Map model to deparment table in the database, it´s use the anotations data
    /// </summary>
    public class Deparment
    {
        public int DeparmentId { get; set; }
        [DisplayName("Departamento")]
        [Required]
        public string Name { get; set; }
        public ICollection<Employee> Employees { get; set; }
    }
}