﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeManager.Models
{
    public class Seeder
    {
        public static void Generate(EmployeeContext context)
        {
            /// seed for departments
            List<string> deparments = new List<string>()
            { "Recursos Humanos", "Contabilidad", "Proyectos", "Almacén", "Seguridad",
                "Desarrollo", "Innovación tecnológica", "Gerencia", "Finanzas", "Mantenimiento" };

            /// seed for employees names
            List<string> names = new List<string>()
            { "Juan", "Pedro", "Maria", "Alejandro", "Jose", "Ana", "Enrique", "Armando", "Aneth", "Nancy",
                "Claudia","Deicy", "Leon", "Isaias", "Arely", "Diana", "Guadalupe","Endra","Evelyn","Sagrario" };

            /// seed for employees last names
            List<string> lastNames = new List<string>()
            {
                "Sotelo", "Carrillo", "Perez", "García", "Serna", "Lara", "Hernandez", "Gonzalez", "Torres", "Salas",
                "Herrera", "Flores", "Macias", "Domingo", "Cruz", "Haro", "Sanchez", "Correa", "Davila", "Betancour"};

            /// Add Deparments in DB if not exists
            if (!context.Deparments.Any())
            {
                try
                {
                    foreach (var name in deparments)
                    {
                        Deparment dep = new Deparment();
                        dep.Name = name;
                        context.Deparments.Add(dep);
                    }
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }

            /// Only if exists departments and not exists employees, add its
            try
            {
                var deparmentCount = context.Deparments.ToList().Count;
                if (deparmentCount > 0 && !context.Employees.Any())
                {
                    int limit = 100;
                    int count = 0;
                    Random r = new Random();

                    while (count < limit)
                    {
                        Employee emp = new Employee();
                        var indexname = r.Next(0, names.Count - 1);
                        var indexLastName = r.Next(0, lastNames.Count - 1);
                        var indexSecondLastName = r.Next(0, lastNames.Count - 1);
                        var indexDepa = r.Next(1, deparmentCount);

                        emp.Name = names[indexname] + " " + lastNames[indexLastName] + " " + lastNames[indexSecondLastName];
                        emp.Name = emp.Name.ToUpper();
                        emp.Email = names[indexname].ToLower() + "_" + lastNames[indexLastName].ToLower() + "@micompany.com";
                        emp.Phone = r.Next(0, 1000000000).ToString("D10");
                        emp.DeparmentId = indexDepa;

                        context.Employees.Add(emp);

                        count++;
                    }

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}