﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace EmployeeManager.Models
{
    public class EmployeeContext: DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public EmployeeContext() : base()
        {
        }

        public System.Data.Entity.DbSet<EmployeeManager.Models.Deparment> Deparments { get; set; }
    }
}