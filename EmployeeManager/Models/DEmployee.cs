﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeManager.Models
{
    /// <summary>
    /// This class get data from databe using quering by Entity Framework
    /// </summary>
    public class DEmployee
    {
        /// <summary>
        ///  Get all employees in DB, the list include departments.
        /// </summary>
        /// <returns></returns>
        public static List<Employee> GetAll(string searchString)
        {
            List<Employee> employees = new List<Employee>();
            try
            {
                using (var db = new EmployeeContext())
                {
                    /// Check filter
                    if (!String.IsNullOrEmpty(searchString))
                    {
                        employees = db.Employees
                            .Include("Deparment")
                            .Where(E => E.Name.Contains(searchString))
                            .OrderBy(I => I.Name)
                            .ToList();
                    }
                    else
                    {
                        /// get with include relationship to Department
                        /// here use clause Order By the EntityFramework for order alphabetize the employees.
                        employees = db.Employees.Include("Deparment").OrderBy(I => I.Name).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return employees;
        }

        /// <summary>
        /// Add a employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public static int Add(Employee employee)
        {
            var result = 0;
            try
            {
                using (var db = new EmployeeContext())
                {
                    db.Employees.Add(employee);
                    result = db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// Search a employee for ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Employee EmployeeFind(int id)
        {
            Employee employee = new Employee();
            try
            {
                using (var db = new EmployeeContext())
                {
                    employee = db.Employees.Include("Deparment").FirstOrDefault(E => E.EmployeeId == id);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return employee;
        }

        /// <summary>
        /// Delete a employee in the Database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Delete(int id)
        {
            bool result = false;
            Employee employee = new Employee();
            try
            {
                using (var db = new EmployeeContext())
                {
                    employee = db.Employees.FirstOrDefault(E => E.EmployeeId == id);
                    if (employee != null)
                    {
                        db.Employees.Remove(employee);
                        int re = db.SaveChanges();
                        if (re > 0)
                            result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// Update a employee
        /// </summary>
        /// <param name="employeeToUpdate"></param>
        /// <returns></returns>
        public static bool Update(Employee employeeToUpdate)
        {
            bool result = false;
            try
            {
                using (var db = new EmployeeContext())
                {
                    Employee currentEmployee = db.Employees.Find(employeeToUpdate.EmployeeId);
                    if (currentEmployee != null)
                    {
                        currentEmployee.Name = employeeToUpdate.Name.ToUpper();
                        currentEmployee.Email = employeeToUpdate.Email;
                        currentEmployee.Phone = employeeToUpdate.Phone;
                        currentEmployee.DeparmentId = employeeToUpdate.DeparmentId;

                        var re = db.SaveChanges();
                        if (re > 0)
                            result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                result = false;
            }
            return result;
        }
    }
}