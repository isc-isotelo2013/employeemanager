﻿using EmployeeManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmployeeManager.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Main grid, the index to page show list employees
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(string searchString)
        {
            try
            {
                /// Get all employees and send list to view
                var employees = DEmployee.GetAll(searchString);
                return View(employees);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return View(new List<Employee>()); /// this return a list empty
        }

        /// <summary>
        ///  this method generate seedr data from web application
        /// </summary>
        /// <returns></returns>
        public ActionResult SeederGenerator()
        {
            try
            {
                using (var db = new EmployeeContext())
                {
                    Seeder.Generate(db);
                }
                TempData["class"] = "alert-success";
                TempData["message"] = "Se completó la operación con éxito.";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                TempData["class"] = "alert-danger";
                TempData["message"] = ex.ToString();
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Get list deparments for fill dropdonw in view
        /// after send to View Create
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            try
            {
                using (var db = new EmployeeContext())
                {
                    var deparments = db.Deparments.OrderBy(D => D.Name).ToList();
                    ViewBag.Deparments = new SelectList(deparments, "DeparmentId", "Name");
                }
                return View();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return HttpNotFound(ex.ToString());
            }
        }

        /// <summary>
        /// Is call when send data from form Create
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                employee.Name = employee.Name.ToUpper();
                int r = DEmployee.Add(employee);
                if (r > 0)
                {
                    TempData["message"] = "Se registró con éxito el empleado.";
                    TempData["class"] = "alert-success";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["message"] = "Error. Ocurrió un problema en el registro del empleado. Contacte al administrador del sistema.";
                    TempData["class"] = "alert-warning";
                    return RedirectToAction("Create");
                }
            }
            else
            {
                TempData["message"] = "Error. Ocurrió un problema en la operación. Verifique la información proporcionada en el formulario.";
                TempData["class"] = "alert-danger";
                return RedirectToAction("Create");
            }
        }

        /// <summary>
        /// This get departments and send to view Edit form
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            try
            {
                using (var db = new EmployeeContext())
                {
                    var deparments = db.Deparments.OrderBy(D => D.Name).ToList();
                    ViewBag.Deparments = new SelectList(deparments, "DeparmentId", "Name");
                }
                Employee employee = DEmployee.EmployeeFind(id);
                return View("Edit", employee);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return HttpNotFound(ex.ToString());
            }

        }

        /// <summary>
        /// Received data from view Edit, this data is for update a employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(Employee employee)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (DEmployee.Update(employee))
                    {
                        TempData["message"] = "Se actualizó con éxito el empleado.";
                        TempData["class"] = "alert-success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["message"] = "Error. Ocurrió un problema en la actualización. Contacte al administrador del sistema";
                        TempData["class"] = "alert-warning";
                        return RedirectToAction("Edit", new { id = employee.EmployeeId });
                    }
                }
                else
                {
                    TempData["message"] = "Error. Ocurrió un problema en la operación. Verifique la información proporcionada en el formulario.";
                    TempData["class"] = "alert-danger";
                    return RedirectToAction("Edit", new { id = employee.EmployeeId });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return HttpNotFound(ex.ToString());
            }
        }

        /// <summary>
        /// Get data for employee to delete, this data is show in view Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            Employee employee = DEmployee.EmployeeFind(id);
            if (employee != null)
            {
                return View("Delete", employee);
            }
            else
                return HttpNotFound();
        }

        /// <summary>
        /// Action for delete employee, that confirmed for the user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirm(int id)
        {
            if (DEmployee.Delete(id))
            {
                TempData["message"] = "Se eliminó con éxito el registro.";
                TempData["class"] = "alert-success";
                return RedirectToAction("Index");
            }
            else
                return HttpNotFound();
        }

        /// <summary>
        /// show dataisl for a employee with id parametrized
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            Employee employee = DEmployee.EmployeeFind(id);
            if (employee != null)
            {
                return View("Detail", employee);
            }
            else
                return HttpNotFound();
        }
    }
}