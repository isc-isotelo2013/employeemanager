namespace EmployeeManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitializeCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        EmployeeId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                        DeparmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EmployeeId)
                .ForeignKey("dbo.Deparments", t => t.DeparmentId, cascadeDelete: true)
                .Index(t => t.DeparmentId);
            
            CreateTable(
                "dbo.Deparments",
                c => new
                    {
                        DeparmentId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.DeparmentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "DeparmentId", "dbo.Deparments");
            DropIndex("dbo.Employees", new[] { "DeparmentId" });
            DropTable("dbo.Deparments");
            DropTable("dbo.Employees");
        }
    }
}
