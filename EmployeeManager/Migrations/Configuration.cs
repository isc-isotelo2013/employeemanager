namespace EmployeeManager.Migrations
{
    using EmployeeManager.Models;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<EmployeeManager.Models.EmployeeContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EmployeeManager.Models.EmployeeContext context)
        {
            Seeder.Generate(context);
        }
    }
}
